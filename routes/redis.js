var redis = require("redis");
var client = redis.createClient();

// if you'd like to select database 3, instead of 0 (default), call
// client.select(3, function() { /* ... */ });
var express=require('express');
var router=express.Router();

router.post('/',function(req,res,next){
  client.on("error", function (err) {
      console.log("Error " + err);
  });

  client.set("string key", "string val", redis.print);
  console.log(client.get("string key"));

  client.get("string ey", function (err, reply) {
      res.send(reply==null);
  });
})


module.exports=router;
