var redis = require("redis");
var client = redis.createClient();
var connectToDB=require('./connection');
var newCon=new connectToDB.connectionPool();


exports = module.exports = function(io){
	io.of("/Message").on('connection',function(socket){
		// console.log('MESSAGE ROUTE');
    try {
          socket.on('chat message', function(msg)
      		{

        			var message=JSON.parse(msg);
        		  	console.log("new message for "+ message.receiver+" "+msg);

        		  	client.get(message.receiver, function (err, reply) {
                     try {
                        io.of("/Message").sockets[reply].emit("chat message", msg);

                     } catch (e) {
                       console.log(e);
                     }

        			});
              newCon.getConnection(function(err,connection){
                 var sql=`insert into Message (Sender,Receiver,Message,Status)
                          values ('${message.sender}','${message.receiver}','${message.content}',0);`;

                 connection.query(sql,function(err,rows){
                   connection.release();
                   if(err){
                     console.error(err);
                   }
                   else{
                     console.log('INSERTED NEW MESSAGE INTO DB SUCCESSFULLY');
                   }
                 });
              });


              client.get(message.sender, function (err, reply) {
                   try {
                      console.log('SERVERACK');
                      io.of("/Message").sockets[reply].emit("serverAck", {
                        "receiver":message.receiver,
                        "UUID": message.UUID
                      });
                     
                   } catch (e) {
                     console.log(e);
                   }
              });
  	      });
    }
    catch (err) {
        console.log(e);
    }


    socket.on('forward',function(msg){
      console.log('SENDING EMOJI');
      var message = JSON.parse(msg);

      console.log(message.receiver);

       client.get(message.receiver, function (err, reply) {
            try {
                io.of("/Message").sockets[reply].emit("forward",msg);
            } catch (e) {
                console.log(e);
            }
          console.log('SETUP MEDIA MESSAGE');

      });
    });

    socket.on('typing',function(msg){
       console.log("TYPING...");
       var message = JSON.parse(msg);
       client.get(message.receiver, function (err, reply) {
          try {
              console.log(`telling ${message.receiver} that i am typing`);
              io.of("/Message").sockets[reply].emit("typing",msg);
          } catch (e) {
              console.log(e);
          }
      });
    });


      socket.on('stoppedTyping',function(msg){
          console.log('STOPPED TYPING...');
          var message = JSON.parse(msg);
          client.get(message.receiver, function (err, reply) {
            try {
                io.of("/Message").sockets[reply].emit("stoppedTyping",msg);
            } catch (e) {
                console.log(e);
            }
      });

    });


    socket.on('recording',function(msg){
       console.log('RECORDING...');
       var message = JSON.parse(msg);
       client.get(message.receiver, function (err, reply) {
          try {
              io.of("/Message").sockets[reply].emit("recording",msg);
          } catch (e) {
              console.log(e);
          }
      });
    });


    socket.on('stoppedRecording',function(msg){
       var message = JSON.parse(msg);
       client.get(message.receiver, function (err, reply) {
          try {
              io.of("/Message").sockets[reply].emit("stoppedRecording",msg);
          }
          catch (e) {
              console.log(e);
          }
      });
    });




    socket.on('online',function(msg){
        var message = JSON.parse(msg)
        console.log(message.user+" is "+'ONLINE');
        client.set(message.user+"-presence", "Online");
    });


    socket.on('offline',function(msg){
        var message = JSON.parse(msg)
        console.log(message.user+" is "+'OFFLINE');
        client.get(message.user, function (err, reply) {
            try {
                io.of("/Message").sockets[reply].emit("offline",msg);
            } catch (e) {
                console.log(e);
            }
          console.log('OFFLINE');
      });
      client.set(message.user+"-presence", "Offline");

    });


		socket.on('setupMediaMessage',function(msg){
			 var message=JSON.parse(msg);
	        //console.log(require('util').inspect(msg, { colors: true }));
	        client.get(message.receiver, function (err, reply) {
            try {
                io.of("/Message").sockets[reply].emit("setupMediaMessage",msg);
            } catch (e) {
                console.log(e);
            }
	   			console.log('SETUP MEDIA MESSAGE');

			});
	    });


    socket.on('clientAck',function(msg){
      var message = JSON.parse(msg)
      client.get(message.sender, function (err, reply) {
        try {
           console.log('CLIENT ACK');
           io.of("/Message").sockets[reply].emit("clientAck",msg);
        } 
        catch (e) {
          console.log(e);
        }
        console.log('SETUP MEDIA MESSAGE');

        });
      });


    socket.on('onlineStatus',function(msg){
        var message = JSON.parse(msg)
        console.log("ONLINE STATUS");
        client.get(message.receiver, function (err, user) {
            client.get(message.receiver+"-presence", function (err, reply) {
                console.log(message.receiver+" is "+reply);
                if(reply=="Online"){
                    try{
                      io.of("/Message").sockets[user].emit("onlineStatus",{"status":reply});
                    }
                    catch(e){
                      console.log(e);
                    }
                }
            });
        });
    });
  

		socket.on('new_user', function(msg)
		{

		  	console.log("new user has joined"+msg);
        // console.log(socket._events);
		  	user=JSON.parse(msg);
		  	var name=user.name;
		    client.set(name, socket.id);
		  	console.log(socket.id);

	  });


	});
};
