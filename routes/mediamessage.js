var redis = require("redis");
var client = redis.createClient();
var express=require('express');
var router=express.Router();
var connectToDB=require('./connection');
var path = require('path');
var multer  = require('multer');
var upload = multer({ dest: path.join(process.cwd(), 'public/uploads/tmp/')});
var fs = require('fs');
var newCon=new connectToDB.connectionPool();
var webp=require('webp-converter');
var connectToDB=require('./connection');
var newCon=new connectToDB.connectionPool();


  router.post('/',upload.single('upl'),function(req,res,next){

    var filename=req.file.filename;
    var sender = req.body.sender;
    var receiver = req.body.receiver;
    var mimetype = req.file.mimetype.substring(0,5);

    var oldPath=process.cwd()+'/public/uploads/tmp/'+filename;
    var newPathVideo=process.cwd()+`/public/uploads/${sender}/media/videos/${filename}`;
    var newPathImage=process.cwd()+`/public/uploads/${sender}/media/images/${filename}`;
    var newPathAudio=process.cwd()+`/public/uploads/${sender}/media/audios/${filename}`;

    /*Compressing images for faster transmission...arguable for the first time sending the
      image, loading the image in at any point in the future will see a faster transmission
      time.*/
    if(mimetype=='image'){
        webp.cwebp(oldPath,newPathImage,"-q 45",function(status){
              //if executed successfully status will be '100'
              //if executed unsuccessfully status will be '101'
              console.log(status);
              if(status=='100'){
                  console.log('SUCCESSFULLY COMPLETED CONVERSION');
              }
        });

    }

    console.log(require('util').inspect(req.file, {colors: true }));
    switch(mimetype){

    /*
        sorting media into it's various folders based on the media type; pictures in
        pictures folder images in images folder and so on.*/
      case 'audio':
          try {
                fs.rename(oldPath,newPathAudio,function(err){
                      if(err){
                              console.log(err);
                      }
                      else{
                              console.log('Successfully saved Voicenote to DISK');
                              // SEND NOTIFICATION TO USER MAKING THEM AWARE THAT AN VOICENOTE IS READY FOR DOWNLOAD
			       client.get(receiver, function(err, reply) {
                               //   io.sockets.sockets[reply].emit();
                              });
                              res.send(mimetype);
                      }
                });

          }
           catch (e) {
                console.log(e);

          }

          break;
    case 'image':
          // SEND NOTIFICATION TO USER MAKING THEM AWARE THAT AN IMAGE IS READY FOR DOWNLOAD
	   client.get(receiver, function(err, reply) {
                                 // io.sockets.sockets[reply].emit();
                              });
          res.send(mimetype);
          break;
    case 'video':
          fs.rename(oldPath,newPathVideo,function(err){
                if(err){
                        console.log(err);
                }
                else{
                        console.log('Successfully saved Video to DISK');
                }
          });
	  client.get(receiver, function(err, reply) {
                                 // io.sockets.sockets[reply].emit();
                              });
          // SEND NOTIFICATION TO USER MAKING THEM AWARE THAT A VIDEO IS READY FOR DOWNLOAD
          res.send(mimetype);

    }



  });




module.exports = router;

