var express=require('express');
var router=express.Router();
var connectToDB=require('./connection');
var newCon=new connectToDB.connectionPool();
path = require('path');
  var webp=require('webp-converter');
var multer  = require('multer');
var upload = multer({ dest: path.join(process.cwd(), 'public/uploads/tmp/')});

router.post('/',upload.single('upl'),function(req,res,next){
  var user = req.body.user;
  var filename=req.file.filename;
  var mimetype = req.file.mimetype.substring(0,5);
  console.log(mimetype);
  var oldPath = process.cwd()+'/public/uploads/tmp/'+filename;
  var newPath = process.cwd()+`/public/uploads/${user}/media/${mimetype}s/${filename}`;


  newCon.getConnection(function(err,connection){
       var sql=`update Users set UserImage="${filename}" where UserID="${user}"`;
       connection.query(sql,function(err,rows){
         connection.release();
         if(err){
           console.error(err);
         }
         else{
           console.log('INSERTED PROFILE PIC INTO DB SUCCESSFULLY');
         }
       });
    });

  /*Compressing images for faster transmission...arguable for the first time sending the
    image, loading the image in at any point in the future will see a faster transmission
    time.*/
    if(mimetype=='image'){
        webp.cwebp(oldPath,newPath,"-q 40",function(status){
              //if executed successfully status will be '100'
              //if executed unsuccessfully status will be '101'
             //  console.log(status);
              if(status=='100'){
                 //  console.log('SUCCESSFULLY COMPLETED CONVERSION');
              }
        });
    }




});
module.exports=router;
