  var express=require('express');
  var app = express();
  var http = require('http').Server(app);
  var io = require('socket.io')(http);
  var path = require('path');
  var morgan = require('morgan');
  var cookieParser = require('cookie-parser');
  var bodyParser = require('body-parser');
  var fs = require('fs');
  var connectToDB=require('./routes/connection');
  var path = require('path');
  var multer  = require('multer');
  var upload = multer({ dest: path.join(process.cwd(), 'public/uploads/tmp/')});
  var newCon=new connectToDB.connectionPool();
  var webp=require('webp-converter');
  var execute = require('child_process').exec;
  var redis = require("redis");
  var client = redis.createClient();

  require('./routes/messaging')(io);
  require('./routes/login')(io);

  io.on('connection',function(socket){
    socket.on('disconnect',function(){
        console.log('USER DISCONNECTED');
    });
  });


  app.get('/sendFriendRequest/:sender/:receiver',function(req,res,next){
    var sender = req.params.sender;
    var receiver = req.params.receiver;

    
    client.get(String(receiver), function (err, reply) {
      try {
              console.log('ABOUT TO EMIT ');
              io.of("/Message").sockets[reply].emit("friendRequest");
          }
          catch (e) {
              console.log(e);

          }
    });
    console.log('DATABASE');

    newCon.getConnection(function(err,connection){
      try {
            var sql=`insert into Relationships (Sender,Receiver,Status) values ('${sender}','${receiver}',0)`;
            connection.query(sql,function(err,rows){
              console.log('FRIEND request');
              console.log(sender+" "+receiver);
              connection.release();
              console.log(require('util').inspect(rows, {colors:true}));
              res.send("OK");
          });

      } catch (e) {
            console.log(e);
      }
  });

  });

  app.post('/mediaMessage/',upload.single('upl'),function(req,res,next){

   console.log(require('util').inspect(req.file, { colors: true}));
    var uuid=req.body.UUID;
    var filename=req.file.filename;
    var sender = req.body.sender;
    var receiver = req.body.receiver;
    var mimetype = req.file.mimetype.substring(0,5);
    var oldPath = process.cwd()+'/public/uploads/tmp/'+filename;
    var newPath = process.cwd()+`/public/uploads/${sender}/media/${mimetype}s/${filename}`;
    var vidThumbnail = `thumbnails/${filename}.jpg`;
    var getMedia = `uploads/${sender}/media/${mimetype}s/${filename}`;

    newCon.getConnection(function(err,connection){
         var sql=`insert into Message (Sender,Receiver,filename,mimetype,screenshot)
         values ('${sender}','${receiver}','${newPath}','${mimetype}','${vidThumbnail}');`;

         connection.query(sql,function(err,rows){
           connection.release();
           if(err){
             console.error(err);
           }
           else{
             console.log('INSERTED NEW MESSAGE INTO DB SUCCESSFULLY');
           }
         });
    });

    /*Compressing images for faster transmission...arguable for the first time sending the
      image, loading the image in at any point in the future will see a faster transmission
      time.*/
    if(mimetype=='image'){
        webp.cwebp(oldPath,newPath,"-q 20",function(status){
              //if executed successfully status will be '100'
              //if executed unsuccessfully status will be '101'
             //  console.log(status);
              if(status=='100'){
                 //  console.log('SUCCESSFULLY COMPLETED CONVERSION');
              }
        });
    }
    switch(mimetype){
    /*  sorting media into it's various folders based on the media type; pictures in
        pictures folder images in images folder and so on.*/
      case 'audio':
          console.log("AUDIO UPLOADED");
          try {
                fs.rename(oldPath,newPath,function(err){
                      if(err){
                               console.log(err);
                      }
                      else{
                           // console.log(require('util').inspect(io.sockets.sockets[connected.jermaine].nsp, { colors:true }));
                           client.get(String(receiver), function (err, reply) {
                             try {
                                     io.of("/Message").sockets[reply].emit("downloadMediaMessage",JSON.stringify(
                                         {
                                           "location":getMedia,
                                           "mimetype":mimetype,
                                           "sender":sender,
                                           "receiver":receiver,
                                           "UUID":uuid
                                         }
                                     ));
                                 }
                                 catch (e) {
                                     console.log(e);

                                 }
                           });
                          
                      }
                });
          }
           catch (e) {
                console.log(e);
          }
          res.send('ok');
          break;
    case 'image':
             console.log("IMAGE UPLODED");
             client.get(String(receiver), function (err, reply) {
               try {
                   io.of("/Message").sockets[reply].emit("downloadMediaMessage",JSON.stringify(
                       {
                         "location":getMedia,
                         "mimetype":mimetype,
                         "sender":sender,
                         "receiver":receiver,
                         "UUID":uuid
                       }
                   ));

               } catch (e) {
                     console.log(e);

               }


             });
         //  client.get(String('jermyhewitt@gmail.com'), function(err, reply) {
         //    io.sockets.sockets[reply].emit('mediamessage',{})
          //
         //  });
       //`console.log(require('util').inspect(io.sockets.sockets, { colors:true }));
       res.send('ok');

          break;
    case 'video':
          
          newCon.getConnection(function(err,connection){
               var sql=`insert into Message (Sender,Receiver,filename,mimetype,screenshot)
               values ('${sender}','${receiver}','${newPath}','${mimetype}','${vidThumbnail}');`;

               connection.query(sql,function(err,rows){
                 connection.release();
                 if(err){
                   console.error(err);
                 }
                 else{
                   console.log('INSERTED NEW MESSAGE INTO DB SUCCESSFULLY');
                 }
               });
          });
          console.log("VIDEO UPLOADED");
          execute(
            `ffmpeg -i ${oldPath} -vframes 1 -s 320x240 -ss 1 ${process.cwd()}/public/thumbnails/${filename}.jpg`, (error, stdout, stderr) => {
              if(error){
                 console.log(`ERROR: ${error}`);
              }
              else{
                    console.log('NO ERRORS! CONTINUING DOWNLOAD OF VIDEO...');
                    res.send('ok');
                    console.log(`EXTRACTING FRAME`);
                    fs.rename(oldPath,newPath,function(err){
                          if(err){
                                  console.log(err);
                          }
                          else{

                                 client.get(String(receiver), function (err, reply) {
                                    console.log('EMITTING DOWNLOAD MEDIA MESSAGE...S');
                                   try {
                                     io.of("/Message").sockets[reply].emit("downloadMediaMessage",JSON.stringify(
                                         {
                                           "mimetype":mimetype,
                                           "sender":sender,
                                           "receiver":receiver,
                                           "UUID":uuid,
                                           "thumbnail":vidThumbnail,
                                           "filename":`uploads/${sender}/media/${mimetype}s/${filename}`
                                         }
                                     ));
                                   } catch (e) {
                                     console.log(e);
                                   }
                                 });


                          }
                    });
              }
            });
           break;
    }



  });

  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'jade');
  app.use(morgan('combined',{immediate: true, format: 'dev'}));

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended:true}));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));

  /*--------------------------------------CATCH 404 AND FORWARD TO ERROR HANDLER--------------------------------------*/
  app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });
  /*--------------------------------------CATCH 404 AND FORWARD TO ERROR HANDLER--------------------------------------*/


  /*--------------------------------------------ERROR HANDLERS--------------------------------------------*/

  // development error handler
  // will print stacktrace
  if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err
      });
    });
  }

  // production error handler
  // no stacktraces leaked to user
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  });
  /*--------------------------------------------ERROR HANDLERS--------------------------------------------*/
  var timeout=http.listen(3001, function () {
    // console.log('Example app listening on port 3000!');
  });
  timeout.setTimeout(10 * 60 * 1000);
  module.exports = app;


// }
