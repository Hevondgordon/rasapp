var express=require('express');
var router=express.Router();
var path = require('path');
var connectToDB=require('./connection');
var newCon=new connectToDB.connectionPool();
var redis = require("redis");
var client = redis.createClient();

router.get('/:UserID',function(req,res,next){
  var UserID=req.params.UserID;
  newCon.getConnection(function(err,connection){
	var sql=`select Users.UserImage,Users.UserID, Users.FirstName,
           Users.LastName,Users.Source from Users where Users.UserID
           in (select Sender from Relationships where Receiver="${UserID}"
           and Status=1);`;
      connection.query(sql,function(err,rows){
          if(err)throw err;
          if(rows.length==0){
              res.send("0");
          }
          else{
            for (var i = 0; i < rows.length; i++) {
              rows[i].Status=1;
            }
            console.log(`COMING FROM DATABASE`);
            res.send(rows);
          }
        });
    });
});

module.exports=router;
