var express=require('express');
var router=express.Router();
var connectToDB=require('./connection');
var fs = require('fs');
var newCon=new connectToDB.connectionPool();
//var transfer = require('transfer-rate');
//var rate = transfer();

function sendResponse(response, responseStatus, responseHeaders, readable) {
    response.writeHead(responseStatus, responseHeaders);

    if (readable == null)
        response.end();
    else
        readable.on('open', function () {
            readable.pipe(response);
        });

    return null;
}

function readRangeHeader(range, totalLength) {
        /*
         * Example of the method 'split' with regular expression.
         *
         * Input: bytes=100-200
         * Output: [null, 100, 200, null]
         *
         * Input: bytes=-200
         * Output: [null, null, 200, null]
         */

    if (range == null || range.length == 0)
        return null;

    var array = range.split(/bytes=([0-9]*)-([0-9]*)/);
    var start = parseInt(array[1]);
    var end = parseInt(array[2]);
    var result = {
        Start: isNaN(start) ? 0 : start,
        End: isNaN(end) ? (totalLength - 1) : end
    };

    if (!isNaN(start) && isNaN(end)) {
        result.Start = start;
        result.End = totalLength - 1;
    }

    if (isNaN(start) && !isNaN(end)) {
        result.Start = totalLength - end;
        result.End = totalLength - 1;
    }

    return result;
}


router.get('/:filename',function(req,response,next){
  var filename = req.params.filename.split('_');
  filename = filename.join('/');
  console.log(filename);
  var responseHeaders={};
  var filename = `${process.cwd()}/public/${filename}`;
  var file = fs.statSync(filename);

  var rangeRequest = readRangeHeader(req.headers['range'], file.size);
  // If 'Range' header exists, we will parse it with Regular Expression.
    if (rangeRequest == null) {
        responseHeaders['Content-Type'] = 'video/mp4';
        responseHeaders['Content-Length'] = file.size;  // File size.
        responseHeaders['Accept-Ranges'] = 'bytes';

        //  If not, will return file directly.
        sendResponse(response, 200, responseHeaders, fs.createReadStream(filename));
        return null;
    }

    var start = rangeRequest.Start;
    var end = rangeRequest.End;

    // If the range can't be fulfilled.
    if (start >= file.size || end >= file.size) {
        // Indicate the acceptable range.
        responseHeaders['Content-Range'] = 'bytes */' + file.size; // File size.

        // Return the 416 'Requested Range Not Satisfiable'.
        sendResponse(response, 416, responseHeaders, null);
        return null;
    }


      responseHeaders['Content-Range'] = 'bytes ' + start + '-' + end + '/' + file.size;
      responseHeaders['Content-Length'] = start == end ? 0 : (end - start + 1);
      responseHeaders['Content-Type'] = 'video/mp4';
      responseHeaders['Accept-Ranges'] = 'bytes';
      responseHeaders['Cache-Control'] = 'no-cache';
      console.log({ "start": start, "end": end });
  // Return the 206 'Partial Content'.
     sendResponse(response, 206, responseHeaders, fs.createReadStream(filename, { start: start, end: end }));

});
module.exports=router;
