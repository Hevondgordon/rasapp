  "use strict";
  var express=require('express');
  var app = express();
  var path = require('path');
  var morgan = require('morgan');
  var cookieParser = require('cookie-parser');
  var bodyParser = require('body-parser');
  var compression = require('compression');
  var connectToDB=require('./routes/connection');
  var path = require('path');
  var multer  = require('multer');
  var upload = multer({ dest: path.join(process.cwd(), 'public/vids/')});
  var fs = require('fs');
  var newCon = new connectToDB.connectionPool();
  var execute = require('child_process').exec;

  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'jade');
  app.use(compression());
  app.use(morgan('combined',{ immediate: true, format: 'dev' }));
  app.use(bodyParser.urlencoded({extended:true}));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));
  

  function getTitle(string){
    try{
      return string.split("-")[1].split('.')[0];
    }
    catch(e){
      return string.split("-")[1];
    }
    
  }


  function getArtiste(string){
    return string.split("-")[0];
  }   


  app.get('/',function(req,res,next){
    res.sendFile(`${process.cwd()}/index.html`)
  }); 

  app.post('/adminUploader', upload.array('upl',12),function(req,res,next){
    console.log(require('util').inspect(req.files, { depth: null }));
    var tdynamic;
    var tboolean
    var genre=req.body.genre;
    var description=req.body.description;
    var year = req.body.year;
    var month = req.body.month;
    var week = req.body.week;
    var files = req.files;
    var data = req.body

    if(req.body.mix!="NULL"){

      var dynamicNboolean=req.body.mix.split("-");
       tdynamic=dynamicNboolean[0];
       tboolean=dynamicNboolean[1];

    }
    else{
      tboolean="NULL";
      tdynamic="dynamic1";
    }
    
    var count=0;

    for (let i = 0; i < req.files.length; i++) {
           fs.rename(`${process.cwd()}/public/vids/${files[i].filename}`, `${process.cwd()}/public/vids/${files[i].originalname}`, function (err) {
              if(err){
                return
              }else{
                execute(
                  `ffmpeg -i '${process.cwd()}/public/vids/${files[i].originalname}' -ss 00:00:14 -vframes 1 '${process.cwd()}/public/thumbnails/${files[i].originalname}.jpg'`,
                    (error, stdout, stderr) => {
                      count+=1;
                      if(count==files.length)res.send(" UPLOAD COMPLETED");
                      if (error) {
                        console.error(error);
                        return;
                      }
                      console.log(`EXTRACTING FRAME NUMBER ${count}`);


                  });
              }

           });
      }
     

      newCon.getConnection(function(err,connection){
           if (err) console.log(err);
           var sql=`insert into Videos(artiste,title,genre,likes,filename,views,${tdynamic},contentTitle,description,year,month,week)
                     values ('${getArtiste(files[0].originalname)}','${getTitle(files[0].originalname)}','${genre}'
                     ,0,'${files[0].originalname}',0,'${tboolean}','${data.contentTitle}','${data.description}',${year},${month},${week})`;

            if(files.length>1){
                for (var i = 1; i < files.length; i++) {

                  sql+=`\n\n,('${getArtiste(files[0].originalname)}','${getTitle(files[0].originalname)}','${genre}'
                 ,0,'${files[i].originalname}',0,'${tboolean}','${data.contentTitle}',
                 '${data.description}',${year},${month},${week})`
                }
            }

           if(tboolean!='NULL'){
             var update=`update Videos set ${tdynamic}='NULL'`;
             connection.query(update,function(err,rows){

                if(err){
                    console.log("error");
                }
                else{
                   console.log("Updated Records");
                }
           });
          try{
            console.log(sql);
            connection.query(sql,function(err,rows){
               connection.release();
               if(err) throw err;
               console.log(err);
               res.send(rows);

             });
          }
          catch(e){
              console.log(e);
          }
             

           }else{
            try{
                connection.query(sql,function(err,rows){
                 connection.release();
               });
            }
            catch(e){
              console.log(e)
            }
           }
      });
  });

  /*--------------------------------------CATCH 404 AND FORWARD TO ERROR HANDLER--------------------------------------*/
  app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });
  /*--------------------------------------CATCH 404 AND FORWARD TO ERROR HANDLER--------------------------------------*/


  /*--------------------------------------------ERROR HANDLERS--------------------------------------------*/

  // development error handler
  // will print stacktrace
  if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err
      });
    });
  }

  // production error handler
  // no stacktraces leaked to user
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  });
  /*--------------------------------------------ERROR HANDLERS--------------------------------------------*/
  var timeout=app.listen(3002, function () {
       console.log('LISTENING FOR UPLOAD ON PORT 3002!');
  });
  timeout.setTimeout(10 * 100 * 1000);
  module.exports = app;
