var express=require('express');
var router=express.Router();
var connectToDB=require('./connection');
var newCon=new connectToDB.connectionPool();

function timeify(first) {

  for (var i = 0; i < first.length; i++) {


    var d1=new Date(first[i].Temporal);
    var d2=new Date();


    var hours = Math.abs(d2 - d1) / 36e5;

    if(hours<24){
      if(Math.floor(hours)==1){
          first[i].Temporal=`${Math.floor(hours)} hr`;
            console.log(`1 HOUR`);
      }
      else if(Math.floor(hours)==0){
          var timeDiff = Math.abs(new Date() - new Date(first[i].Temporal));
          var minutes = Math.floor((timeDiff/1000)/60);
          if(minutes==1){
              first[i].Temporal=`${minutes} min`;
          }
   else if(minutes<1){
              var seconds = (d2.getTime() - d1.getTime()) / 1000;
    seconds=Math.floor(seconds);
                first[i].Temporal=`${seconds} secs`;

          }
    else{
              first[i].Temporal=`${minutes} mins`;
          }



      }
      else if (hours>1) {
            first[i].Temporal=`${Math.floor(hours)} hrs`;
      }


    }

    else if((Math.floor(hours)/24)==1){
          first[i].Temporal=`${Math.floor(hours)/24} day`;

    }
    else if(hours>24 & hours<48){
      first[i].Temporal=`${Math.floor(hours/24)} day`;

    }
    else if(hours>=48){
        first[i].Temporal=`${Math.floor(hours/24)} days ago`;

    }
    else{

    }



  }

}

router.get('/:UserID/:viewerID',function(req,res,next){

  var UserID=req.params.UserID
  var viewerID=req.params.viewerID;

  newCon.getConnection(function(err,connection){
    var sql=`select distinct Users.FirstName,Users.LastName,Users.UserImage,Posts.CommentCount,Posts.Rotation,Posts.PostID as innervar,
           (select Users.LastName from Users right join Likes on Users.UserID=Likes.UserID where Likes.PostID=innervar limit 1) as
            likeLName,(select Users.FirstName from Users right join Likes on Users.UserID=Likes.UserID where Likes.PostID=innervar limit 1)
            as likeFName,(select Liked from Likes where UserID='${viewerID}' and PostID=innervar) as iLike,Posts.PostID,Posts.mimetype,
            Posts.UserID,Posts.Temporal,Posts.Image,Posts.LikeCount,Posts.Description,Posts.Rank from Users left join Likes on Users.UserID=Likes.UserID
            join Posts on Posts.UserID=Users.UserID where Posts.UserID in (select UserID from Posts where UserID='${UserID}') order by Temporal desc`;
    connection.query(sql,function(err,rows){
 	if(err){
		console.log('error in getprofile.js');
	}else{
    timeify(rows);
		res.send(rows);

	}

    });
  });

});

module.exports=router;
