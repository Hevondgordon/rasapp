var connectToDB=require('./connection');
var bcrypt = require('bcrypt');
const saltRounds = 10;
var newCon=new connectToDB.connectionPool();
var fs = require('fs');
exports = module.exports = function(io){


  io.of("/login").on('connection',function(socket){

  socket.on('login',function(msg){
    console.log('LOGIN');
  	console.log(require('util').inspect(msg, { colors:true }));
    var user=JSON.parse(msg);
    newCon.getConnection(function(err,connection){
    connection.query(`select Password,FirstName,LastName from Users where UserID='${user.Username}'`,function(err,rows){
      if(rows.length==0){
          //INCORRECT USERNAME
          console.log('INCORRECT USERNAME');
      }
      else{
        bcrypt.compare(user.Password, rows[0].Password, function(err, res) {
            if(res){
              socket.emit("loginResult",{
                "firstName":rows[0].FirstName,
                "lastName":rows[0].LastName,
                "status":1
            });

            }else{
              //INCORRECT PASSWORD
              console.log('INCORRECT PASSWORD');
            }


        });

      }
    });


  });
});



  socket.on('register',function(msg){
      console.log('REGISTER');
      console.log(require('util').inspect(msg, { colors:true }));
      var user=JSON.parse(msg);
      var password=user.Password;


    newCon.getConnection(function(err,connection){
          if(user.Source!='Google' && user.Source!='Facebook'){
            bcrypt.genSalt(saltRounds, function(err, salt) {
                bcrypt.hash(password, salt, function(err, hash) {
                  try{
                        var sql= `insert into Users (UserID,LastName,FirstName,Password) values ('${user.Username}','${user.Lastname}','${user.Firstname}','${hash}');`;

                        connection.query(sql,function(err,rows){
                          if(err){
                              console.log(err);
                              socket.emit('regis','ErrorCode:2');
                          }
                          else{

                              var pathTo = `${process.cwd()}/public/uploads/${user.Username}/`;
                              socket.emit('regis','Success:1');
                              console.log('successful login using original method');
                              fs.mkdir(`${pathTo}/videos`,function(err){
                                 console.log('CREATED VIDEOS DIRECTORY');
                               });

                              fs.mkdir(`${pathTo}/images`,function(err){
                              console.log('CREATED IMAGES DIRECTORY');
                              });

                              fs.mkdir(`${pathTo}/media`,function(err){
                                console.log('CREATED MEDIA DIRECTORY');
                                fs.mkdir(`${pathTo}/media/audios`,function(err){
                                  console.log('CREATED MEDIA/VOICENOTES DIRECTORY');
                                });
                                fs.mkdir(`${pathTo}/media/images`,function(err){
                                  console.log('CREATED MEDIA/IMAGES DIRECTORY');
                                });
                                fs.mkdir(`${pathTo}/media/videos`,function(err){
                                  console.log('CREATED MEDIS/VIDEOS DIRECTORY');
                                });
                              });
                          }
                        });
                  }
                  catch(e){

                  }

                });
          });
        }
        else{
            var sql= `insert into Users (UserID,LastName,FirstName,UserImage,source) values
                      ('${user.Username}','${user.Lastname}','${user.Firstname}','${user.Image}',
                      '${user.Source}')`;

            try{
              connection.query(sql,function(err,rows){
                  if(err){
                      console.log('logging in with fb or google...');
                      socket.emit('regis','Success:1');
                  }
                  else{
                        var pathTo = `${process.cwd()}/public/uploads/${user.Username}/`;
                        socket.emit('regis','Success:1');
                        console.log('successful registration using fb or google');
                         fs.mkdir(`${pathTo}/videos`,function(err){
                                 console.log('CREATED VIDEOS DIRECTORY');
                               });

                              fs.mkdir(`${pathTo}/images`,function(err){
                              console.log('CREATED IMAGES DIRECTORY');
                              });

                              fs.mkdir(`${pathTo}/media`,function(err){
                                console.log('CREATED MEDIA DIRECTORY');
                                fs.mkdir(`${pathTo}/media/audios`,function(err){
                                  console.log('CREATED MEDIA/VOICENOTES DIRECTORY');
                                });
                                fs.mkdir(`${pathTo}/media/images`,function(err){
                                  console.log('CREATED MEDIA/IMAGES DIRECTORY');
                                });
                                fs.mkdir(`${pathTo}/media/videos`,function(err){
                                  console.log('CREATED MEDIS/VIDEOS DIRECTORY');
                                });
                              });
                        var relationship = `insert into Relationships (Sender,Receiver,Status) values ('${user.Username}','${user.Username}',3)`;
                          connection.query(relationship,function(err,rows){
                                connection.release();
                                  if(err){
                                      console.log('RELATIONSHIPS ERROR');
                                  }
                                  else{
                                        console.log('me to me');
                                  }
                           });
                  }
             });

            }
            catch(e){
                console.log(e);
            }
        }
      });


});
});


}
