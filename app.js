var cluster = require('cluster');

if(cluster.isMaster) {
    var numWorkers = require('os').cpus().length;

    console.log('Master cluster setting up ' + numWorkers + ' workers...');

    for(var i = 0; i < numWorkers; i++) {
        cluster.fork();
    }

    cluster.on('online', function(worker) {
         console.log('Worker ' + worker.process.pid + ' is online');
    });

    cluster.on('exit', function(worker, code, signal) {
         console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
         console.log('Starting a new worker');
        cluster.fork();
    });
} else {

  var express=require('express');
  var app = express();
  var path = require('path');
  var morgan = require('morgan');
  var cookieParser = require('cookie-parser');
  var bodyParser = require('body-parser');
  var compression = require('compression');

  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'jade');
  app.use(compression());
  app.use(morgan('combined',{ immediate: true, format: 'dev' }));
  app.use(bodyParser.urlencoded({extended:true}));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));

  // ENDPOINTS
  app.use('/getComments',require('./routes/getcomments'));
  app.use('/getDynamic1',require('./routes/getdynamic1'));
  app.use('/getDynamic2',require('./routes/getdynamic2'));
  app.use('/getDynamicData',require('./routes/getdynamicdata'));
  app.use('/getGenre',require('./routes/getgenre'));
  app.use('/getLatest',require('./routes/getlatest'));
  app.use('/getLikes',require('./routes/getlikes'));
  app.use('/getTrending',require('./routes/gettrending'));
  app.use('/getVideo',require('./routes/getvideo'));
  app.use('/getAd',require('./routes/getad'));
  app.use('/getAdVid',require('./routes/getadvid'));
  app.use('/like',require('./routes/like'));
  app.use('/newsfeed',require('./routes/newsfeed'));
  app.use('/upload',require('./routes/upload'));
  app.use('/genreThumbnail',require('./routes/genrethumbnail'));
  app.use('/getMainVid',require('./routes/getmainvid'));
  app.use('/genreThumbnail',require('./routes/genrethumbnail'));
  app.use('/comment',require('./routes/comment'));
  app.use('/getRelated',require('./routes/getrelated'));
  app.use('/getAccessories',require('./routes/getaccessories'));
  app.use('/setRecentContacts',require('./routes/setrecentcontacts'));
  app.use('/getVideoSearch',require('./routes/getvideosearch'));
  app.use('/paypal',require('./routes/paypal'));
  app.use('/updateViews',require('./routes/updateviews'));
  app.use('/getNewsfeedVid',require('./routes/getnewsfeedvid'));
  app.use('/deletePost',require('./routes/deletepost'));
  app.use('/comment',require('./routes/comment'));
  app.use('/setPlaylist',require('./routes/setplaylist'));
  app.use('/getPlaylist',require('./routes/getplaylist'));
  app.use('/getArtiste',require('./routes/getartiste'));
  app.use('/getContacts',require('./routes/getcontacts'));
  app.use('/findContacts',require('./routes/findcontacts'));
  app.use('/getFriendRequests',require('./routes/getfriendrequest'));
  app.use('/voiceNote',require('./routes/voicenote'));
  app.use('/getProfile', require('./routes/getprofile'));
  app.use('/getMessageVid', require('./routes/getmessagevid'));
  app.use('/setProfilePicture', require('./routes/setprofilepicture'));
  app.use('/getProfilePicture', require('./routes/getprofilepicture'));
  app.use('/getRecentContacts', require('./routes/getrecentcontacts'));
  app.use('/acceptFriendRequest', require('./routes/acceptfriendrequest'));
  app.use('/getEmoji', require('./routes/emoji.js'));
  app.use('/getVoiceNotes', require('./routes/getvoicenotes'));
  
    


  /*--------------------------------------CATCH 404 AND FORWARD TO ERROR HANDLER--------------------------------------*/
  app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });
  /*--------------------------------------CATCH 404 AND FORWARD TO ERROR HANDLER--------------------------------------*/


  /*--------------------------------------------ERROR HANDLERS--------------------------------------------*/

  // development error handler
  // will print stacktrace
  if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err
      });
    });
  }

  // production error handler
  // no stacktraces leaked to user
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  });
  /*--------------------------------------------ERROR HANDLERS--------------------------------------------*/
  var timeout=app.listen(3000, function () {
    // console.log('Example app listening on port 3000!');
  });
  timeout.setTimeout(10 * 60 * 1000);
  module.exports = app;


}
