var express = require('express');
var router = express.Router();
var connectToDB=require('./connection');
var path = require('path');
var fs = require('fs');
var newCon = new connectToDB.connectionPool();
var webp = require('webp-converter');
const execute = require('child_process').exec;
var multer  = require('multer');
var upload = multer({ dest: path.join(process.cwd(), 'public/uploads/tmp/')});
var util = require('util');


router.post('/:id',upload.single('upl'),function(req,res,next){
  console.log("SETTING UP VARIABLES...");
  var userID = req.params.id;
  var pathTo = `${process.cwd()}/public/uploads/${userID}/`;
  var tmp = process.cwd()+'/public/uploads/tmp';
  var filename = req.file.filename;
  var rotation = parseInt(req.body.rotation);
  var mimetype = req.file.mimetype.substring(0,5);
  var description = req.body.Description;
  var oldPath = process.cwd()+`/public/uploads/tmp/${filename}`;
  var newPath = process.cwd()+`/public/uploads/${userID}/${mimetype}s/${filename}`;
  var pathToThumbnails = process.cwd()+'/public/thumbnails/'+filename;

  // CHECK THE AMONT OF FREINDS THAT A USER HAS AND SHOW THEM THE PUBLIC OR PRIVATE FEED
  // 5 FRIEMNDS GET A PUBLIC FEED. MORE FRIENDS GET A PRIVATE FEED
  //AIM IS TO HAVE SOME FORM OF INTERACTION AT EVERY POINT IN TIME

  newCon.getConnection(function(err,connection){

       var sql=`select count(Receiver) as count from Relationships where Sender="${userID}"`;
       connection.query(sql,function(err,rows){
         connection.release();
         if(rows[0].count<4){
	           console.log('\n\nPUBLIC FEED HANDLER\n\n');
             require('./uploadpublic')(description,userID,filename,mimetype,rotation,res);

         }
         else{
		         console.log('\n\nPRIVATE FEED HANDLER\n\n');
             require('./uploadprivate')(description,userID,filename,mimetype,rotation,res);
         }
       });

    });


    /*---------------------------------------CHECK IF THE DIRECTORY EXISTS AND THEN MOVE FILE TO IT---------------------------------------*/

 fs.stat(pathTo, function(err, fileStat) {
     if (err) {
       if (err.code == 'ENOENT') {
         try {
             fs.mkdir(pathTo,function(err){
             fs.mkdir(`${pathTo}/videos`,function(err){
               console.log('CREATED VIDEOS DIRECTORY');
             });

            fs.mkdir(`${pathTo}/images`,function(err){
            console.log('CREATED IMAGES DIRECTORY');
            });

            fs.mkdir(`${pathTo}/media`,function(err){
              console.log('CREATED MEDIA DIRECTORY');
              fs.mkdir(`${pathTo}/media/audios`,function(err){
                console.log('CREATED MEDIA/VOICENOTES DIRECTORY');
              });
              fs.mkdir(`${pathTo}/media/images`,function(err){
                console.log('CREATED MEDIA/IMAGES DIRECTORY');
              });
              fs.mkdir(`${pathTo}/media/videos`,function(err){
                console.log('CREATED MEDIS/VIDEOS DIRECTORY');
              });
            });

            if(mimetype=="image"){
              webp.cwebp(oldPath,newPath,"-q 30",function(status){
                //if exicuted successfully status will be '100'
                //if exicuted unsuccessfully status will be '101'
                fs.unlink(oldPath, function(err){
                  if(err){
                    console.log('could not delete file for whatever reason')
                  }
                  else{
                    console.log('cleaned up...');
                  }
                });

                if(parseInt(status)==100){
                  console.log('SUCCESSFULLY COMPLETED CONVERSION');
                  res.send('ok')
                }
              });
            }
            else{
              if(mimetype="video"){
                execute(`ffmpeg -i ${oldPath} -vframes 1 -s 320x240 -ss 1 ${pathToThumbnails}.jpg`, (error, stdout, stderr) => {
                  if (error) {
                    console.error(error);
                  }
                  console.log(`EXTRACTED FRAME!`);
                  fs.rename(oldPath, newPath, function (err) {
                    if (err) throw err;
                    console.log('Move complete after creating new folder! ELSE CASE! NOT IMAGE');
                    fs.unlink(oldPath, function(err){
                    if(err){
                      console.log('could not delete file for whatever reason')
                    }
                    else{
                      console.log('cleaned up...')
                    }
                    });
                  });
                });
              }
            }
         });
        }
        catch (e) {
          console.log('error on uploading files! check the rename on new user condition')
       }
     }
   }
    else {
      if (fileStat.isDirectory()) {
      console.log('Directory found.');
      if(mimetype=="image"){
        webp.cwebp(oldPath,newPath,"-q 30",function(status){
        //if executed successfully status will be '100'
        //if executed unsuccessfully status will be '101'
          fs.unlink(oldPath, function(err){
            if(err){
            console.log('could not delete file for whatever reason')
            }
            else{
            console.log('cleaned up...')
            }
          });

          if(parseInt(status)==100){
            console.log('SUCCESSFULLY COMPLETED CONVERSION');
          }
        });
      }
      else{
        if(mimetype="video"){
          execute(`ffmpeg -i ${oldPath} -vframes 1 -s 320x240 -ss 1 ${pathToThumbnails}.jpg`, (error, stdout, stderr) => {
            try{
               console.log(`EXTRACTED FRAME!`);
                fs.rename(oldPath, newPath, function (err) {
                  fs.unlink(oldPath, function(err){
                    if(err){
                    console.log('could not delete file for whatever reason')
                    }
                    else{
                     console.log('cleaned up...')
                    }
                  });
                  console.log('Move complete! folder already existed! ELSE CASE! NOT IMAGE');
                });

            }
            catch(e){
              console.log(e);
            }
          
          });

        }
      }
  }
  }
  });

     /*---------------------------------------CHECK IF THE DIRECTORY EXISTS AND THEN MOVE FILE TO IT---------------------------------------*/
});


module.exports = router;
